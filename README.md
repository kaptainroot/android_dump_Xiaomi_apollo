## qssi-user 12
12 SKQ1.211006.001 V13.0.3.0.SJDMIXM release-keys
- Manufacturer: xiaomi
- Platform: kona
- Codename: apollo
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 12
12
- Kernel Version: 4.19.157
- Id: SKQ1.211006.001
- Incremental: V13.0.3.0.SJDMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Treble Device: true
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/apollo_global/apollo:12/RKQ1.211001.001/V13.0.3.0.SJDMIXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.211006.001-V13.0.3.0.SJDMIXM-release-keys
- Repo: redmi_apollo_dump
